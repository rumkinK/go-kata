package solution

import (
	"errors"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.Merged = map[string]string{}
	emptyDict := false
	for _, item := range job.Dicts {
		if item != nil {
			for key, value := range item {
				job.Merged[key] = value
			}
		} else {
			emptyDict = true
		}
	}
	job.IsFinished = true
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}
	if emptyDict {
		return job, errNilDict
	}
	return job, nil
}
