package main

import "testing"

var (
	users    = genUsers()
	products = genProducts()
)

func BenchmarkSample(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSample2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
