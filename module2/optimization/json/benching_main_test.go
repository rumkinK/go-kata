package main

import (
	"io/ioutil"
	"os"
	"testing"
)

func BenchmarkStandartJsonUnMarshal(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	f, _ := os.Open("pets.json")
	defer f.Close()
	jsonData, _ := ioutil.ReadAll(f)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
		_, _ = data, pets
	}
}
func BenchmarkStandartJsonMarshal(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
func BenchmarkJsonIterUnMarshal(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	f, _ := os.Open("pets.json")
	defer f.Close()
	jsonData, _ := ioutil.ReadAll(f)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
		_, _ = data, pets
	}
}
func BenchmarkJsonIterMarshal(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
