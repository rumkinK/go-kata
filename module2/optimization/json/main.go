// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    pets, err := UnmarshalPets(bytes)
//    bytes, err = pets.Marshal()

package main

import (
	"encoding/json"
	jsoniter "github.com/json-iterator/go"
)

type Pets []Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}
func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}
func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  *Category  `json:"category,omitempty"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Category struct {
	ID   float64 `json:"id"`
	Name string  `json:"name"`
}

type Status string

const (
	Pending Status = "pending"
)
