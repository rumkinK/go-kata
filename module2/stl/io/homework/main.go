// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	proverbs := new(bytes.Buffer)
	for _, item := range data {
		proverbs.WriteString(item + "\n")
	}
	f, err := os.Create("example.txt")
	check(err)
	proverbs.WriteTo(f)
	f.Close()
	f, err = os.Open("example.txt")
	check(err)
	defer f.Close()
	newBuff := new(bytes.Buffer)
	io.Copy(newBuff, f)
	fmt.Println(newBuff)
}
