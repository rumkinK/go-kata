package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

type Config struct {
	AppName    string
	Production bool
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	var filename string
	conf := Config{}
	flag.StringVar(&filename, "conf", "config.json", "Указать файл с конфигурацией JSON")
	flag.Parse()
	if filename != "" {
		f, err := os.Open(filename)

		if err != nil {
			fmt.Println("error opening file: err:", err)
			os.Exit(1)
		}
		defer f.Close()
		data, err := ioutil.ReadAll(f)
		check(err)
		if err := json.Unmarshal(data, &conf); err != nil {
			fmt.Println(err)
			return
		}
	}
	fmt.Println(conf)
}
