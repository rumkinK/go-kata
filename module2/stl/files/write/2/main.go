package main

import (
	"io/ioutil"
	"os"
	"pkg.re/essentialkaos/translit.v2"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	fContent, err := ioutil.ReadFile("example.txt")
	check(err)
	dataTranslit := translit.EncodeToBGN(string(fContent))
	f, err := os.Create("example.processed.txt")
	check(err)
	defer f.Close()
	_, err = f.WriteString(dataTranslit)
	check(err)
}
