package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 108000,
		},
		{
			Name:  "https://github.com/jlevy/the-art-of-command-line",
			Stars: 125000,
		},
		{
			Name:  "https://github.com/juspay/hyperswitch",
			Stars: 1100,
		},
		{
			Name:  "https://github.com/novuhq/novu",
			Stars: 17500,
		},
		{
			Name:  "https://github.com/sveltejs/kit",
			Stars: 12700,
		},
		{
			Name:  "https://github.com/sickcodes/Docker-OSX",
			Stars: 28000,
		},
		{
			Name:  "https://github.com/wangkechun/go-by-example",
			Stars: 461,
		},
		{
			Name:  "https://github.com/williamyang1991/VToonify",
			Stars: 2500,
		},
		{
			Name:  "https://github.com/AUTOMATIC1111/stable-diffusion-webui",
			Stars: 28700,
		},
		{
			Name:  "https://github.com/pi-hole/pi-hole",
			Stars: 40900,
		},
		{
			Name:  "https://github.com/lencx/ChatGPT",
			Stars: 5100,
		},
		{
			Name:  "https://github.com/Grasscutters/Grasscutter",
			Stars: 12300,
		},
	}
	mapPrj := make(map[string]Project)
	for i := range projects {
		mapPrj[projects[i].Name] = projects[i]
	}

	for key, item := range mapPrj {
		fmt.Println(key, item)
	}
}
