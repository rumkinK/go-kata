package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData(ctx context.Context) chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			case <-ctx.Done():
				return
			}
		}
	}()

	return out
}

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData(ctx)

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}

	//time.Sleep(3 * time.Second)

}
