package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	t := time.Now()
	rand.Seed(t.UnixNano())

	go parseUrl("www.example.com")
	parseUrl("www.youtube.com")

	fmt.Printf("Parsing completed. Timr elapsed %.2f seconds\n", time.Since(t).Seconds())
}
func parseUrl(url string) {
	for i := 0; i <= 5; i++ {
		latency := rand.Intn(500) + 500
		time.Sleep(time.Duration(latency) * time.Millisecond)
		fmt.Printf("Parsing <%s> step %d - Latency %d ms\n", url, i+1, latency)
	}
}
