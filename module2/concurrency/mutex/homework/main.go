package main

import (
	"fmt"
	"golang.org/x/sync/errgroup"
	"sync"
)

type Cache struct {
	mutex sync.Mutex
	data  map[string]interface{}
	init  bool
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}) error {
	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	c.mutex.Lock()
	c.data[key] = v
	c.mutex.Unlock()
	return nil
}

func (c *Cache) Get(key string) interface{} {
	if !c.init {
		return nil
	}
	c.mutex.Lock()
	value := c.data[key]
	c.mutex.Unlock()
	return value
}

func main() {
	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}

	var eg errgroup.Group
	for i := range keys {
		idx := i
		eg.Go(func() error {
			return cache.Set(keys[idx], idx)
		})
	}

	err := eg.Wait()
	if err != nil {
		panic(err)
	}
}
