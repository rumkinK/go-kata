package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData(done chan bool) chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case <-done:
				return
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

const duration = 30

func main() {
	ticker := time.NewTicker(duration * time.Second)
	done := make(chan bool)
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData(done)

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)
	//функция ждет пока сработает тикер, как только тикер срабоает в out перестают поступать данные
	//и соответственно все каналы закроются. После этого в консоль выведется объединенный канал
	go func() {
		for {
			select {
			case <-ticker.C:
				done <- true
			}
		}
	}()

	/*go func() {
		for value := range mainChan {
			select {
			case <-done:
				fmt.Println("HERE")
				close(mainChan)
				return
			case <-mainChan:
				fmt.Println(value)
			case <-ticker.C:
				fmt.Println("STOP")
				done <- true

			}
		}
	}()*/

	/*go func() {
		for {
			select {
			case <-done:
				fmt.Println("here!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
				return
			case <-ticker.C:
				//close(a)
				fmt.Println("DONE")
				time.Sleep(1 * time.Second)
				done <- true

			case value := <-mainChan:
				fmt.Println(value)
			}
		}
	}()*/

	time.Sleep(duration * time.Second)
	ticker.Stop()

	for num := range mainChan {
		fmt.Println(num)
	}

	time.Sleep(3 * time.Second)
}
