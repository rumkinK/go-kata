package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	(*c).a = a
	return c
}

func (c *calc) SetB(b float64) *calc {
	(*c).b = b

	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	(*c).result = operation(c.a, c.b)

	return c
}

func (c *calc) Result() float64 {
	return (*c).result
}

func Multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func Divade(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	if b == 0 {
		return 0
	} else {
		return ((a * 10) / (b)) / 10
	}
}

func Sum(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a + b
}

func Average(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return (a + b) / 2
}

func main() {
	calc := NewCalc()
	res := calc.SetA(10).SetB(34).Do(Multiply).Result()
	fmt.Println(res)
	res2 := calc.Result()
	fmt.Println(res2)
	if res != res2 {
		panic("object statement is not persist")
	}
}
