package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMultiply(t *testing.T) {
	type testCase struct {
		name     string
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 2, b: 2, expected: 4},
		{a: 1, b: 4, expected: 4},
		{a: 0, b: 4, expected: 0},
		{a: 2, b: 0, expected: 0},
		{a: 1.5, b: 2, expected: 3},
		{a: 10, b: 2.3, expected: 23},
		{a: 1.2, b: 4, expected: 4.8},
		{a: 1.5, b: 4.5, expected: 6.75},
	}
	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Multiply(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("case : a = %f, b = %f while expected result equals to %f, "+
			"received result equals to %f\n", test.a, test.b, test.expected, receivedResult)
	}
}

func TestDivade(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 2, b: 2, expected: 1},
		{a: 1, b: 2, expected: 0.5},
		{a: 0, b: 4, expected: 0},
		{a: 2, b: 0, expected: 0},
		{a: 4, b: 2, expected: 2},
		{a: 10, b: 2.5, expected: 4.0},
		{a: 1.2, b: 3.0, expected: 0.4},
		{a: 2.5, b: 5, expected: 0.5},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Divade(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("case : a = %f, b = %f while expected result equals to %f, "+
			"received result equals to %f\n", test.a, test.b, test.expected, receivedResult)
	}
}
func TestSum(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 2, b: 2, expected: 4},
		{a: 1, b: 2, expected: 3},
		{a: 0, b: 4, expected: 4},
		{a: 2, b: 0, expected: 2},
		{a: 4, b: 2.5, expected: 6.5},
		{a: 6.2, b: 2.5, expected: 8.7},
		{a: 1.2, b: 3.0, expected: 4.2},
		{a: 2.5, b: 0.5, expected: 3},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Sum(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("case : a = %f, b = %f while expected result equals to %f, "+
			"received result equals to %f\n", test.a, test.b, test.expected, receivedResult)
	}
}
func TestAverage(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 2, b: 2, expected: 2},
		{a: 1, b: 2, expected: 1.5},
		{a: 0, b: 4, expected: 2},
		{a: 2, b: 0, expected: 1},
		{a: 4, b: 1, expected: 2.5},
		{a: 10, b: 2.5, expected: 6.25},
		{a: 1.2, b: 3.0, expected: 2.1},
		{a: 2.5, b: 5, expected: 3.75},
		{a: 2.5, b: 5, expected: 3.75},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Average(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("case : a = %f, b = %f while expected result equals to %f, "+
			"received result equals to %f\n", test.a, test.b, test.expected, receivedResult)
	}
}
