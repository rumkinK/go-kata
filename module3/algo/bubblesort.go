package algo

func BubbleSort(data []int) []int {
	i := 0
	swapped := true
	for swapped {
		swapped = false
		for j := 0; j < len(data)-1-i; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true
			}
		}
		i++
	}
	return data
}
