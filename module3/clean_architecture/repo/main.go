package main

import (
	"fmt"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/repo/repository"
	"log"
	"os"
)

func main() {
	file, err := os.OpenFile("./users.json", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0755)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	repo := repository.NewUserRepository(file)
	if err := repo.Save(repository.User{
		ID:   1,
		Name: "Sam",
	}); err != nil {
		log.Println(err)
	}

	if err := repo.Save(repository.User{
		ID:   2,
		Name: "Max",
	}); err != nil {
		log.Println(err)
	}

	if err := repo.Save(repository.User{
		ID:   3,
		Name: "Dave",
	}); err != nil {
		log.Println(err)
	}

	oneUser, err := repo.Find(2)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(oneUser.(repository.User).ID, oneUser.(repository.User).Name)

	allUsers, err := repo.FindAll()
	if err != nil {
		log.Println(err)
	}

	for _, user := range allUsers {
		fmt.Printf("ID: %d, Name: %s\n", user.(repository.User).ID, user.(repository.User).Name)
	}

}
