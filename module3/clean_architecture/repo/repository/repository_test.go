package repository

import (
	"io"
	"log"
	"os"
	"reflect"
	"testing"
)

var testFile = func() (*os.File, error) {
	file, err := os.CreateTemp("", "users.json")
	if err != nil {
		return nil, err
	}
	defer os.Remove(file.Name())
	return file, err
}

func TestUserRepository_Find(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}
	repo := NewUserRepository(file)
	if err := repo.Save(User{
		ID:   1,
		Name: "Sam",
	}); err != nil {
		log.Println(err)
	}
	if err := repo.Save(User{
		ID:   2,
		Name: "Max",
	}); err != nil {
		log.Println(err)
	}
	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name: "valid users",
			fields: fields{
				File: file,
			},
			args: args{id: 2},
			want: User{
				ID:   2,
				Name: "Max",
			},
			wantErr: false,
		},
		{
			name: "invalid users",
			fields: fields{
				File: file,
			},
			args:    args{id: 5},
			want:    nil,
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}
	repo := NewUserRepository(file)
	if err := repo.Save(User{
		ID:   1,
		Name: "Sam",
	}); err != nil {
		log.Println(err)
	}
	if err := repo.Save(User{
		ID:   2,
		Name: "Max",
	}); err != nil {
		log.Println(err)
	}
	type fields struct {
		File io.ReadWriteSeeker
	}
	tests := []struct {
		name    string
		fields  fields
		want    []interface{}
		wantErr bool
	}{
		{
			name: "all users",
			fields: fields{
				File: file,
			},
			want: []interface{}{
				User{
					ID:   1,
					Name: "Sam",
				},
				User{
					ID:   2,
					Name: "Max",
				},
			},
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_Save(t *testing.T) {
	file, err := testFile()
	if err != nil {
		log.Println(err)
	}
	type fields struct {
		File io.ReadWriteSeeker
	}
	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "save element",
			fields: fields{File: file},
			args: args{
				record: User{
					ID:   1,
					Name: "Max",
				},
			},
			wantErr: false,
		},
		{
			name:   "add duplicate element",
			fields: fields{File: file},
			args: args{
				record: User{
					ID:   1,
					Name: "Max",
				},
			},
			wantErr: true,
		},
		{
			name:   "invalid record",
			fields: fields{File: file},
			args: args{
				record: "Empty",
			},
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &UserRepository{
				File: tt.fields.File,
			}
			if err := r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
