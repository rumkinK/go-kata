package cli

import (
	"fmt"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/entity"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/service"
	"log"
	"os"
)

type Cli struct {
	service service.Service
}

func NewCli(service *service.Service) *Cli {
	return &Cli{service: *service}
}

func (c *Cli) Run() {
	var menu string

	fmt.Print("Выберите меню:\na. Показать список задач\nb. Добавить задачу\nc. Удалить задачу\nd. Редактировать задачу по ID\ne. Завершить задачу\nf. Exit\n")
	for {
		fmt.Scan(&menu)
		switch menu {
		case "a":
			c.List()
		case "b":
			c.Add()
		case "c":
			c.Remove()
		case "d":
			c.Update()
		case "e":
			c.Complete()
		case "f":
			os.Exit(0)
		default:
			fmt.Println("Ошибка ввода!")
		}
	}
}

func (c *Cli) List() {
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println("Tasks:")
	for _, task := range tasks {
		status := "[ ]"
		if task.Completed {
			status = "[X]"
		}
		fmt.Printf("%d. %s %s: %s\n", task.ID, status, task.Title, task.Description)
	}
}

func (c *Cli) Add() {
	var description, tit string
	fmt.Print("Ведите название и описание: ")
	fmt.Scanln(&tit, &description)
	task, err := c.service.Add(tit, description)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Задача  №%d %s добавлена\n", task.ID, task.Title)
}

func (c *Cli) Update() {
	var idTask int
	var selectedTask entity.Task
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Print("Введите ID задачи для редактирования: ")
	fmt.Scanln(&idTask)

	for _, task := range tasks {
		if task.ID == idTask {
			selectedTask = task
			break
		}
	}
	var description, tit string
	fmt.Print("Ведите название и описание: ")
	fmt.Scanln(&tit, &description)
	selectedTask.Title = tit
	selectedTask.Description = description

	task, err := c.service.Update(selectedTask)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Задача №:%d Имя: %s изменена\n", task.ID, task.Title)
}

func (c *Cli) Remove() {
	var idTask int
	var selectedTask entity.Task
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Print("Введите ID задачи для удаления: ")
	fmt.Scanln(&idTask)

	for _, task := range tasks {
		if task.ID == idTask {
			selectedTask = task
			break
		}
	}
	_, err = c.service.Remove(selectedTask.ID)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Задача %d %s удалена\n", selectedTask.ID, selectedTask.Title)
}

func (c *Cli) Complete() {
	var idTask int
	var selectedTask entity.Task
	tasks, err := c.service.List()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Print("Введите ID задачи для завершения: ")
	fmt.Scanln(&idTask)

	for _, task := range tasks {
		if task.ID == idTask {
			selectedTask = task
			break
		}
	}
	_, err = c.service.Complete(selectedTask.ID)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("Задача %d %s завершена\n", selectedTask.ID, selectedTask.Title)
}

/*func (c *Cli) Run() {
	for {
		var choice string

		prompt := &survey.Select{
			Message: "Task Menu:",
			Options: []string{"List", "Add", "Update", "Complete", "Remove", "Exit"},
		}
		if err := survey.AskOne(prompt, &choice); err != nil {
			return
		}

		switch choice {
		case "List":
			c.List()
		case "Add":
			c.Add()
		case "Update":
			c.Update()
		case "Complete":
			c.Complete()
		case "Remove":
			c.Remove()
		case "Exit":
			return
		}
	}
}








*/
