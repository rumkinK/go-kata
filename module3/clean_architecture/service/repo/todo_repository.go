package repo

import (
	"encoding/json"
	"fmt"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/entity"
	"log"
	"os"
)

type FileDB struct {
	FilePath string
}

func NewFileDB(filePath string) *FileDB {
	return &FileDB{
		FilePath: filePath,
	}
}

func (fdb FileDB) Add(title, description string) (entity.Task, error) {
	tasks, err := fdb.List()
	if err != nil {
		return entity.Task{}, err
	}
	var task entity.Task

	task.ID = len(tasks) + 1
	task.Title = title
	task.Description = description

	tasks = append(tasks, task)

	if err := fdb.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

func (fdb FileDB) Remove(id int) (int, error) {
	tasks, err := fdb.List()
	if err != nil {
		return id, err
	}
	var found bool
	for i, t := range tasks {
		if t.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)

			found = true

			break
		}
	}

	if !found {
		return id, fmt.Errorf("Задача не найдена")
	}

	if err := fdb.SaveTasks(tasks); err != nil {
		return id, err
	}

	return id, nil
}

func (fdb FileDB) Update(task entity.Task) (entity.Task, error) {
	tasks, err := fdb.List()
	if err != nil {
		return task, err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	if err := fdb.SaveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

func (fdb FileDB) Complete(id int) (int, error) {
	tasks, err := fdb.List()
	found := false
	if err != nil {
		return id, err
	}
	for i, t := range tasks {
		if t.ID == id {
			tasks[i].Completed = true
			found = true
			break
		}
	}

	if !found {
		return id, fmt.Errorf("Задача не найдена")
	}

	if err = fdb.SaveTasks(tasks); err != nil {
		return id, err
	}

	return id, nil
}

func (fdb FileDB) List() ([]entity.Task, error) {
	var tasks []entity.Task

	file, err := os.OpenFile(fdb.FilePath, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Println(err)
		}
	}(file)

	checkSize, _ := os.Stat(fdb.FilePath)
	if checkSize.Size() == 0 {
		return nil, nil
	}

	decoder := json.NewDecoder(file)

	if err := decoder.Decode(&tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

func (fdb FileDB) SaveTasks(tasks []entity.Task) error {
	f, err := os.OpenFile(fdb.FilePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}

	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Println(err)
		}
	}(f)

	encoder := json.NewEncoder(f)
	return encoder.Encode(tasks)
}
