package entity

type Task struct {
	ID          int
	Title       string
	Description string
	Completed   bool
}
