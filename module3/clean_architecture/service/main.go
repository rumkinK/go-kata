package main

import (
	cli "gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/cli"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/service"
)

func main() {
	repo := repo.NewFileDB("tasks.json")
	service := service.NewService(repo)
	cli := cli.NewCli(service)

	cli.Run()
}
