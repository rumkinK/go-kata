package tests

import (
	"encoding/json"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/entity"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/service"
	"log"
	"os"
	"reflect"
	"testing"
)

func init() {
	_ = os.Remove("test_task.json")

	f, err := os.OpenFile("test_task.json", os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(f)
	var tasks = []entity.Task{
		{
			ID:          1,
			Title:       "task1",
			Description: "desc1",
			Completed:   false,
		},
		{
			ID:          2,
			Title:       "task2",
			Description: "desc2",
			Completed:   false,
		},
		{
			ID:          3,
			Title:       "task3",
			Description: "desc3",
			Completed:   true,
		},
	}
	encoder := json.NewEncoder(f)
	if err := encoder.Encode(tasks); err != nil {
		log.Fatal(err)
	}
}

func TestService_List(t *testing.T) {
	type fields struct {
		repo service.TaskRepo
	}
	tests := []struct {
		name    string
		fields  fields
		want    []entity.Task
		wantErr bool
	}{
		{
			name: "List",
			fields: fields{
				repo: repo.NewFileDB("test_task.json"),
			},
			want: []entity.Task{
				{
					ID:          1,
					Title:       "task1",
					Description: "desc1",
					Completed:   false,
				},
				{
					ID:          2,
					Title:       "task2",
					Description: "desc2",
					Completed:   false,
				},
				{
					ID:          3,
					Title:       "task3",
					Description: "desc3",
					Completed:   true,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)

			got, err := s.List()
			if (err != nil) != tt.wantErr {
				t.Errorf("List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("List() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_Add(t *testing.T) {
	type fields struct {
		repo service.TaskRepo
	}
	type args struct {
		title       string
		description string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Task
		wantErr bool
	}{
		{
			name: "Add task",
			fields: fields{
				repo: repo.NewFileDB("test_task.json"),
			},
			args: args{
				title:       "task4",
				description: "desc4",
			},
			want: entity.Task{
				ID:          4,
				Title:       "task4",
				Description: "desc4",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)
			got, err := s.Add(tt.args.title, tt.args.description)
			if (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Add() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_Complete(t *testing.T) {
	type fields struct {
		repo service.TaskRepo
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "Complete task",
			fields: fields{
				repo: repo.NewFileDB("test_task.json"),
			},
			args: args{
				id: 1,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)

			got, err := s.Complete(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Complete() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Complete() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_Remove(t *testing.T) {
	type fields struct {
		repo service.TaskRepo
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "Remove task",
			fields: fields{
				repo: repo.NewFileDB("test_task.json"),
			},
			args: args{
				id: 1,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)

			got, err := s.Remove(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Remove() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Remove() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_Update(t *testing.T) {
	type fields struct {
		repo service.TaskRepo
	}
	type args struct {
		task entity.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Task
		wantErr bool
	}{
		{
			name: "Update task",
			fields: fields{
				repo: repo.NewFileDB("test_task.json"),
			},
			args: args{
				task: entity.Task{
					ID:          1,
					Title:       "task2",
					Description: "desc2",
					Completed:   false,
				},
			},
			want: entity.Task{
				ID:          1,
				Title:       "task2",
				Description: "desc2",
				Completed:   false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewService(tt.fields.repo)

			got, err := s.Update(tt.args.task)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}
