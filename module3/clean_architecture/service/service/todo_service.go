package service

import "gitlab.com/rumkinK/go-kata/module3/clean_architecture/service/entity"

type TaskRepo interface {
	Add(title, description string) (entity.Task, error)
	Remove(id int) (int, error)
	Update(task entity.Task) (entity.Task, error)
	Complete(id int) (int, error)
	List() ([]entity.Task, error)
}

type Service struct {
	repo TaskRepo
}

func NewService(repo TaskRepo) *Service {
	return &Service{repo: repo}
}

func (s Service) Add(title, description string) (entity.Task, error) {
	return s.repo.Add(title, description)
}

func (s Service) Remove(id int) (int, error) {
	return s.repo.Remove(id)
}

func (s Service) Update(task entity.Task) (entity.Task, error) {
	return s.repo.Update(task)
}

func (s Service) Complete(id int) (int, error) {
	return s.repo.Complete(id)
}

func (s Service) List() ([]entity.Task, error) {
	return s.repo.List()
}
