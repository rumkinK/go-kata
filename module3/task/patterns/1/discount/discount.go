package discount

type PricingStrategy interface {
	Calculate(order Order) float64
}

type RegularPricing struct {
	Title string
}

type SalePricing struct {
	Title    string
	Discount float64
}

type Order struct {
	Name  string
	Price float64
	Count int
}

func (r RegularPricing) Calculate(order Order) float64 {
	cost := order.Price
	return cost
}

func (s SalePricing) Calculate(order Order) float64 {
	cost := order.Price * float64(order.Count) * (1 - s.Discount)
	return cost
}
