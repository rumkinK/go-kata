package main

import (
	"fmt"
	"gitlab.com/rumkinK/go-kata/module3/task/patterns/1/discount"
)

func main() {
	order := discount.Order{
		Name:  "Xerox DC250",
		Price: 300000,
		Count: 5,
	}

	strat := []discount.PricingStrategy{
		discount.RegularPricing{Title: "Цена без скидки"},
		discount.SalePricing{Title: "Скидка в кибер неделю", Discount: 0.15},
		discount.SalePricing{Title: "Скидка в честь дня рождения", Discount: 0.10},
		discount.SalePricing{Title: "Новогодняя распродажа", Discount: 0.05},
	}

	for _, strategy := range strat {
		cost := strategy.Calculate(order)
		fmt.Printf("%v %.2f\n", strategy, cost)
	}
}
