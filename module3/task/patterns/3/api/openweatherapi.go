package api

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

type WeatherAPI interface {
	WeatherRequest(location string) (int, int, float64)
}

type WeatherInfo struct {
	Fact struct {
		Temp      int     `json:"temp,omitempty"`
		Humidity  int     `json:"humidity,omitempty"`
		WindSpeed float64 `json:"wind_speed,omitempty"`
	} `json:"fact,omitempty"`
}

type YandexWeatherAPI struct {
	apiKey string
}

func (o *YandexWeatherAPI) WeatherRequest(location string) (int, int, float64) {
	// Yandex weather API url
	c := GetCoordinates(location, CSVMap)
	url := fmt.Sprintf("https://api.weather.yandex.ru/v2/forecast?lat=%v&lon=%v&extra=false", c[0], c[1])

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("X-Yandex-API-Key", o.apiKey)

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err)
	}
	defer res.Body.Close()

	body, _ := io.ReadAll(res.Body)

	var weatherInfo WeatherInfo
	err = json.Unmarshal(body, &weatherInfo)
	if err != nil {
		log.Println(err)
	}
	return weatherInfo.Fact.Temp, weatherInfo.Fact.Humidity, weatherInfo.Fact.WindSpeed
}

type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (f *WeatherFacade) GetWeatherInfo(location string) (int, int, float64) {
	return f.weatherAPI.WeatherRequest(location)
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &YandexWeatherAPI{apiKey: apiKey},
	}
}
