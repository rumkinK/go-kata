package api

import (
	"encoding/csv"
	"os"
)

var CSVMap = make(map[string][]string)

func CSVReader(filepath string) error {
	csvFile, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer csvFile.Close()

	if _, err := csvFile.Seek(0, 0); err != nil {
		return err
	}
	reader := csv.NewReader(csvFile)
	reader.Comma = ','

	rawData, err := reader.ReadAll()
	if err != nil {
		return err
	}
	for _, record := range rawData {
		CSVMap[record[0]] = []string{record[1], record[2]}
	}
	return nil
}

func GetCoordinates(location string, cities map[string][]string) []string {
	coordinats := cities[location]
	return coordinats
}
