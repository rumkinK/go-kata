package main

import (
	"fmt"
	"gitlab.com/rumkinK/go-kata/module3/task/patterns/3/api"
)

func init() {
	var err = api.CSVReader("cities.csv")
	if err != nil {
		panic(err)
	}
}

func main() {
	weatherFacade := api.NewWeatherFacade("e67e38d9-b74a-40f8-9ed6-51593b9e0643")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %.02f\n\n", windSpeed)
	}
	weatherFacade.GetWeatherInfo("Москва")
}
