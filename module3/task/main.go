package task

import (
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"
)

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

type Node struct {
	Data *Commit
	prev *Node
	next *Node
}

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("error reading file")
	}
	defer func() {
		err := f.Close()
		if err != nil {
			log.Println("error closing file")
		}
	}()

	data, err := ioutil.ReadAll(f)

	if err != nil {
		log.Fatal(err)
		return err
	}

	commits := []Commit{}
	jsonErr := json.Unmarshal(data, &commits) //получаем данные из json

	if jsonErr != nil {
		log.Fatal(jsonErr)
		return jsonErr
	}

	commits = QuickSort(commits) // сортируем полученные данные
	for c := range commits {     // в цикле заполняем список данными из массива
		d.Append(&commits[c])
	}
	return nil
}

func (d *DoubleLinkedList) Append(c *Commit) *DoubleLinkedList {
	if c == nil {
		return d
	}
	n := &Node{Data: c}
	if d.head == nil {
		d.head, d.curr, d.tail = n, n, n // First node
	} else {
		d.tail.next = n // Add after prev last node
		n.prev = d.tail // Link back to prev last node
		d.curr = d.tail
	}
	d.tail = n // reset tail to newly added node
	d.curr = d.tail
	d.len++
	return d
}

func QuickSort(arr []Commit) []Commit {
	if len(arr) <= 1 {
		return arr
	}
	median := arr[rand.Intn(len(arr))].Date
	low_part := make([]Commit, 0, len(arr))
	high_part := make([]Commit, 0, len(arr))
	middle_part := make([]Commit, 0, len(arr))
	for _, item := range arr {
		switch {
		case median.After(item.Date):
			low_part = append(low_part, item)
		case median.Equal(item.Date):
			middle_part = append(middle_part, item)
		case median.Before(item.Date):
			high_part = append(high_part, item)
		}
	}
	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)
	low_part = append(low_part, middle_part...)
	low_part = append(low_part, high_part...)
	return low_part
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	if d.curr == nil {
		return nil
	}
	if d.curr == d.head {
		return d.curr
	}
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil || d.head == nil {
		return nil
	}
	if d.curr == d.tail {
		return d.curr
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil || d.head == nil {
		return nil
	}
	if d.curr == d.head {
		return d.curr
	}
	d.curr = d.curr.prev
	return d.curr
}

func (d *DoubleLinkedList) JumpTo(n int) (*Node, error) {
	if d == nil {
		return nil, fmt.Errorf("cannot jump to nil list")
	}

	if n < 0 || n > d.Len()-1 {
		return nil, fmt.Errorf("index out of range")
	}
	if n <= (d.Len()-1)/2 {
		d.curr = d.head
		for i := 0; i < n; i++ {
			d.curr = d.curr.next
		}
	} else {
		d.curr = d.tail
		for i := d.Len() - 1; i > n; i-- {
			d.curr = d.curr.prev
		}
	}
	return d.curr, nil
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c *Commit) error {
	if d == nil {
		return fmt.Errorf("cannot insert nil list")
	}
	if c == nil {
		return fmt.Errorf("cannot insert nil commit")
	}
	if n < 0 || n > d.Len() {
		return fmt.Errorf("index out of range")
	}

	curr, err := d.JumpTo(n)
	if err != nil {
		return err
	}

	newNode := &Node{Data: c, prev: curr, next: curr.next}
	curr.next = newNode
	if newNode.next == nil {
		d.tail = newNode
	} else {
		newNode.next.prev = newNode
	}

	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	_, err := d.JumpTo(n)
	if err != nil {
		return err
	}

	err = d.DeleteCurrent()
	if err != nil {
		return err
	}

	return err
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	switch {
	case d.curr == nil:
		return fmt.Errorf("current element is empty")
	case d.curr == d.head:
		d.curr = d.head.next
		d.curr.prev = nil
		d.head = d.curr
	case d.curr == d.tail:
		d.tail.prev.next = nil
		d.tail = d.tail.prev
		d.curr = d.tail
	default:
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.curr = d.curr.prev
	}
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("current node not in list")
	}
	idx := 0
	indexing := d.head
	for indexing != d.curr {
		indexing = indexing.next
		idx++
	}
	return idx, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.tail == nil {
		return nil
	}
	node := d.tail
	if d.head == d.tail {
		d.head, d.curr, d.tail = nil, nil, nil
	} else {
		d.tail = d.tail.prev
		d.tail.next = nil
	}
	d.len--
	return node
}

// Shift операция shift убрать первый элемент списка
func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}
	node := d.head
	if d.head == d.tail {
		d.head, d.curr, d.tail = nil, nil, nil
	} else {
		d.head = d.head.next
		d.head.prev = nil
	}
	d.len--
	return node
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	found := false
	var ret *Node = nil
	for n := d.head; n != nil && !found; n = n.next {
		if n.Data.UUID == uuID {
			found = true
			ret = n
		}
	}
	return ret
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	found := false
	var ret *Node = nil
	for n := d.head; n != nil && !found; n = n.next {
		if n.Data.Message == message {
			found = true
			ret = n
		}
	}
	return ret
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.head == nil || d.head.next == nil {
		return d
	}
	curr := d.head
	for curr != nil {
		curr.prev, curr.next = curr.next, curr.prev
		curr = curr.prev
	}
	d.head, d.tail = d.tail, d.head
	return d
}

func GenerateJSON(amount int) error {
	var commits []Commit
	for i := 0; i < amount; i++ {
		commits = append(commits, Commit{
			Message: gofakeit.LoremIpsumSentence(6),
			UUID:    gofakeit.UUID(),
			Date:    time.Now().Add(time.Duration(rand.Intn(300)) * time.Second),
		})
	}
	jsonData, err := json.Marshal(commits)
	if err != nil {
		return err
	}
	err = os.WriteFile("data.json", jsonData, 0644)
	if err != nil {
		return err
	}
	return nil
}
