package main

import (
	"fmt"
)

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct {
	temperature int
	active      bool
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
	log           []string
}

func NewAirConditionerProxy(auth bool, log []string) *AirConditionerProxy {
	return &AirConditionerProxy{
		adapter: &AirConditionerAdapter{
			airConditioner: &RealAirConditioner{
				active: false,
			},
		},
		authenticated: auth,
		log:           log,
	}
}

func (r *RealAirConditioner) TurnOn() {
	r.active = true
}

func (r *RealAirConditioner) TurnOff() {
	r.active = false
}

func (r *RealAirConditioner) SetTemperature(t int) {
	r.temperature = t
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a *AirConditionerAdapter) SetTemperature(t int) {
	a.airConditioner.SetTemperature(t)
}

func (p *AirConditionerProxy) TurnOn() {
	if p.authenticated {
		p.adapter.airConditioner.TurnOn()
		fmt.Println("Turning on the air conditioner")
		p.log = append(p.log, "Authenticated access: turning on the air conditioner")
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
		p.log = append(p.log, "Turning on failed: authentication required")
	}
}

func (p *AirConditionerProxy) TurnOff() {
	if p.authenticated {
		p.adapter.airConditioner.TurnOff()
		fmt.Println("Turning off the air conditioner")
		p.log = append(p.log, "Authenticated access: turning off the air conditioner")
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
		p.log = append(p.log, "Turning off failed: authentication required")
	}
}
func (p *AirConditionerProxy) SetTemperature(t int) {
	if p.authenticated {
		p.adapter.airConditioner.SetTemperature(t)
		fmt.Printf("Setting temperature to %d\n", t)
		p.log = append(p.log, "Authenticated access: setting temperature")
	} else {
		fmt.Println("Access denied: authentication required to set the temperature")
		p.log = append(p.log, "Setting temperature failed: authentication required")
	}
}

func main() {
	var accessLog []string

	airConditioner := NewAirConditionerProxy(false, accessLog) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true, accessLog) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
