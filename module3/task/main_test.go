package task

import (
	"reflect"
	"testing"
	"time"
)

func TestDoubleLinkedList_Append(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	type args struct {
		c *Commit
	}
	want := &DoubleLinkedList{
		head: head,
		curr: head,
		tail: head,
		len:  1,
	}
	want2 := &DoubleLinkedList{
		head: head,
		curr: second,
		tail: second,
		len:  2,
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		len    int
		want   *DoubleLinkedList
	}{
		{
			name: "add to empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				c: &Commit{
					Message: "Head",
					UUID:    "1",
					Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
				},
			},
			len:  1,
			want: want,
		},
		{
			name: "append",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			args: args{
				c: &Commit{
					Message: "second",
					UUID:    "2",
					Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
				},
			},
			len:  2,
			want: want2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Append(tt.args.c); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Append() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: head,
				len:  0,
			},
			want: head,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: second,
				len:  1,
			},
			want: second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	type args struct {
		n int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		len     int
		wantErr bool
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				n: 0,
			},
			len:     0,
			wantErr: true,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			args: args{
				n: 1,
			},
			len:     1,
			wantErr: true,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			args: args{
				n: 1,
			},
			len:     2,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Delete(tt.args.n); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
		next: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
		next: nil,
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "delete nil",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			wantErr: true,
		},
		{
			name: "del current",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Index(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr bool
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want:    -1,
			wantErr: true,
		},
		{
			name: "head list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			want:    0,
			wantErr: false,
		},
		{
			name: "some list2",
			fields: fields{
				head: head,
				tail: second,
				curr: head,
				len:  2,
			},
			want:    0,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Index()
			if (err != nil) != tt.wantErr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Index() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
		next: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
		next: nil,
	}

	type args struct {
		n int
		c *Commit
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "add to empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args: args{
				c: &Commit{
					Message: "Head",
					UUID:    "1",
					Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
				},
				n: 1,
			},
			wantErr: true,
		},
		{
			name: "insert second",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			args: args{
				c: &Commit{
					Message: "second",
					UUID:    "2",
					Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
				},
				n: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.Insert(tt.args.n, tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: head,
				len:  0,
			},
			want: 0,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: second,
				len:  1,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			want: head,
		},
		{
			name: "some list2",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			want: second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			want: head,
		},
		{
			name: "some list2",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			want: second,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Pop(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			want: head,
		},
		{
			name: "some list2",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			want: head,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	type args struct {
		c *Commit
	}
	want := &DoubleLinkedList{
		head: head,
		curr: head,
		tail: head,
		len:  1,
	}
	want2 := &DoubleLinkedList{
		head: head,
		curr: second,
		tail: second,
		len:  2,
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		len    int
		want   *DoubleLinkedList
	}{
		{
			name: "revers 1 list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			args: args{
				c: &Commit{
					Message: "Head",
					UUID:    "1",
					Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
				},
			},
			len:  1,
			want: want,
		},
		{
			name: "some list element",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			args: args{
				c: &Commit{
					Message: "second",
					UUID:    "2",
					Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
				},
			},
			len:  2,
			want: want2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Reverse(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
	}
	type args struct {
		uuID string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "search 1 list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			args: args{
				uuID: "1",
			},
			want: head,
		},
		{
			name: "some list element",
			fields: fields{
				head: head,
				tail: second,
				curr: second,
				len:  2,
			},
			args: args{
				uuID: "2",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.SearchUUID(tt.args.uuID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var head = &Node{
		Data: &Commit{
			Message: "Head",
			UUID:    "1",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: nil,
		next: nil,
	}
	var second = &Node{
		Data: &Commit{
			Message: "second",
			UUID:    "2",
			Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
		},
		prev: head,
		next: nil,
	}
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "empty list",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
		{
			name: "some list",
			fields: fields{
				head: head,
				tail: head,
				curr: head,
				len:  1,
			},
			want: head,
		},
		{
			name: "some list2",
			fields: fields{
				head: head,
				tail: head,
				curr: second,
				len:  2,
			},
			want: head,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Shift(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}
		})
	}
}
