package task

import (
	"log"
	"testing"
	"time"
)

var Path = "./test.json"

func makeTestData() *DoubleLinkedList {
	d := &DoubleLinkedList{}
	err := d.LoadData(Path)
	if err != nil {
		log.Fatal(err)
	}
	return d
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := d.LoadData(Path); err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkDoubleLinkedList_Append(b *testing.B) {
	d := makeTestData()
	commit := &Commit{
		Message: "Head",
		UUID:    "1",
		Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := d.Append(commit); err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = d.Current()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = d.Next()
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = d.Prev()
	}
}
func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = d.Len()
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if _, err := d.Index(); err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if err := d.DeleteCurrent(); err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if err := d.Delete(1); err != nil {
			log.Fatal(err)
		}
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()
	for i := 0; i < 99; i++ {
		_ = d.Pop()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()
	for i := 0; i < 99; i++ {
		_ = d.Shift()
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	d := makeTestData()
	b.ResetTimer()
	for i := 0; i < 99; i++ {
		_ = d.Reverse()
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	d := makeTestData()
	commit1 := &Commit{
		Message: "Head",
		UUID:    "1",
		Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
	}
	commit2 := &Commit{
		Message: "second",
		UUID:    "2",
		Date:    time.Date(2017, 4, 25, 8, 5, 2, 0, time.Local),
	}
	_ = d.Insert(25, commit1)
	_ = d.Insert(75, commit2)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = d.SearchUUID("1")
		_ = d.SearchUUID("2")
	}
}
