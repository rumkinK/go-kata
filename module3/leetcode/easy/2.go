package main

import "fmt"

func main() {
	test := []int{1, 2, 1}
	fmt.Println(getConcatenation(test))
}

func getConcatenation(nums []int) []int {
	cap := len(nums)
	r := make([]int, cap*2, cap*2)
	for i := range nums {
		r[i], r[i+cap] = nums[i], nums[i]
	}
	return r
}
