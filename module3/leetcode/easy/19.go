package main

import (
	"fmt"
	"sort"
)

func main() {
	n := 2932
	fmt.Println(minimumSum(n))
}

func minimumSum(num int) int {
	var nums []int
	for num > 0 {
		nums = append(nums, num%10)
		num = num / 10
	}
	sort.Ints(nums)
	one := nums[0]*10 + nums[2]
	two := nums[1]*10 + nums[3]
	return one + two
}
