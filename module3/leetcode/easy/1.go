package main

import "fmt"

func main() {
	fmt.Println(tribonacci(25))
}

func tribonacci(n int) int {
	cap := 3
	if n > 2 {
		cap = n + 1
	}
	trib := make([]int, cap, cap)
	trib[0] = 0
	trib[1] = 1
	trib[2] = 1
	for i := 3; i <= n; i++ {
		trib[i] = trib[i-1] + trib[i-2] + trib[i-3]
	}
	return trib[n]
}
