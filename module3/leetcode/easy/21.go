package main

import (
	"fmt"
)

func main() {
	cand := []int{2, 3, 5, 1, 3}
	extra := 3
	fmt.Println(kidsWithCandies(cand, extra))
}

func kidsWithCandies(candies []int, extraCandies int) []bool {
	great := 0
	for _, numCandies := range candies {
		if numCandies > great {
			great = numCandies
		}
	}
	res := make([]bool, 0, len(candies))
	for _, numCandies := range candies {
		res = append(res, (great <= numCandies+extraCandies))
	}
	return res
}
