package main

import (
	"fmt"
	"strings"
)

func main() {
	opr := []string{"--X", "X++", "X++"}
	fmt.Println(finalValueAfterOperations(opr))
}

func finalValueAfterOperations(operations []string) int {
	res := 0
	for _, v := range operations {
		if strings.Contains(v, "+") {
			res += 1
		} else {
			res -= 1
		}
	}
	return res
}
