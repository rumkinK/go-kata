package main

import (
	"fmt"
)

func main() {
	num := []int{1, 2, 3, 4}
	fmt.Println(decompressRLElist(num))
}

func decompressRLElist(nums []int) []int {
	var res []int
	for i := 0; i < len(nums); i++ {
		for j := 0; j < nums[i]; j++ {
			res = append(res, nums[i+1])
		}
		i++
	}
	return res
}
