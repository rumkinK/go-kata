package main

import (
	"fmt"
)

func main() {
	num := []int{1, 2, 3, 1, 1, 3}
	fmt.Println(numIdenticalPairs(num))
}

func numIdenticalPairs(nums []int) int {
	m := make(map[int]int, len(nums))
	amount := 0
	for _, v := range nums {
		if val, ok := m[v]; ok {
			amount += val
		}
		m[v] = m[v] + 1
	}
	return amount
}
