package main

import (
	"fmt"
)

func main() {
	n := 5
	fmt.Println(smallestEvenMultiple(n))
}

func smallestEvenMultiple(n int) int {
	if n%2 == 0 {
		return n
	}
	return n * 2
}
