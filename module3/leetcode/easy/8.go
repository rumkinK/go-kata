package main

import (
	"fmt"
	"sort"
)

func main() {
	a := []int{2, 3, 4, 7, 11}
	k := 5
	fmt.Println(findKthPositive(a, k))
}

func findKthPositive(arr []int, k int) int {
	res := k + sort.Search(len(arr), func(i int) bool { return arr[i] > k+i })
	return res
}
