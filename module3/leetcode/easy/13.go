package main

import (
	"fmt"
)

func main() {
	jew := "aA"
	ston := "aAAbbbb"
	fmt.Println(numJewelsInStones(jew, ston))
}

func numJewelsInStones(jewels string, stones string) int {
	m := make(map[int32]int)
	for _, v := range stones {
		m[v]++
	}
	res := 0
	for _, v := range jewels {
		if m[v] != 0 {
			res += m[v]
		}
	}
	return res
}
