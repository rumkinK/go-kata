package main

import "fmt"

func main() {
	cel := 36.5
	fmt.Println(convertTemperature(cel))
}

func convertTemperature(celsius float64) []float64 {
	tempConvert := make([]float64, 2, 2)
	tempConvert[0], tempConvert[1] = celsius+273.15, celsius*1.80+32.00
	return tempConvert
}
