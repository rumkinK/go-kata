package main

import (
	"fmt"
)

func main() {
	n := 7
	fmt.Println(countDigits(n))
}

func countDigits(num int) int {
	a := num
	count := 0
	for a > 0 {
		val := a % 10
		if num%val == 0 {
			count++
		}
		a /= 10
	}
	return count
}
