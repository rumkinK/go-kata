package main

import "fmt"

func main() {
	nums := []int{0, 2, 1, 5, 3, 4}
	fmt.Println(nums)
	fmt.Println(buildArray(nums))
}
func buildArray(nums []int) []int {
	res := make([]int, len(nums), len(nums))
	for i := range nums {
		res[i] = nums[nums[i]]
	}
	return res
}
