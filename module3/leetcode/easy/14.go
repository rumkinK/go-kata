package main

import (
	"fmt"
)

func main() {
	account := [][]int{{1, 2, 3}, {3, 2, 1}}
	fmt.Println(maximumWealth(account))
}

func maximumWealth(accounts [][]int) int {
	for i := 0; i < len(accounts); i++ {
		for j := 1; j < len(accounts[i]); j++ {
			accounts[i][0] += accounts[i][j]
		}
		if accounts[i][0] > accounts[0][0] {
			accounts[0][0] = accounts[i][0]
		}
	}
	return accounts[0][0]
}
