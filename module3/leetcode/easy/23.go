package main

import (
	"fmt"
	"strings"
)

func main() {
	com := "G()(al)"
	fmt.Println(interpret(com))
}

func interpret(command string) string {
	o := strings.Contains(command, "()")
	al := strings.Contains(command, "(al)")
	if o {
		command = strings.ReplaceAll(command, "()", "o")
	}
	if al {
		command = strings.ReplaceAll(command, "(al)", "al")
	}
	return command
}
