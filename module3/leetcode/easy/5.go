package main

import "fmt"

func main() {
	n := 14
	fmt.Println(numberOfMatches(n))
}

func numberOfMatches(n int) int {
	return n - 1
}
