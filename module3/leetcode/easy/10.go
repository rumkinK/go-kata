package main

import (
	"fmt"
)

func main() {
	num := []int{2, 5, 1, 3, 4, 7}
	n := 3
	fmt.Println(shuffle(num, n))
}

func shuffle(nums []int, n int) []int {
	res := []int{}
	for i := 0; i < n; i++ {
		res = append(res, nums[i], nums[i+n])
	}
	return res
}
