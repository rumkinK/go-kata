package main

import (
	"fmt"
)

func main() {
	s := "RLRRLLRLRL"
	fmt.Println(balancedStringSplit(s))
}

func balancedStringSplit(s string) int {
	count := 0
	l := 0
	for _, val := range s {
		if val == 'L' {
			l++
		} else {
			l--
		}
		if l == 0 {
			count++
		}
	}
	return count
}
