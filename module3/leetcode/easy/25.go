package main

import (
	"fmt"
)

func main() {
	n := []int{0, 1, 2, 3, 4}
	idx := []int{0, 1, 2, 2, 1}
	fmt.Println(createTargetArray(n, idx))
}

func createTargetArray(nums []int, index []int) []int {
	res := make([]int, 0, len(nums))
	for i, v := range index {
		res = append(res[:v+1], res[v:]...)
		res[v] = nums[i]
	}
	return res
}
