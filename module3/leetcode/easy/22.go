package main

import (
	"fmt"
)

func main() {
	n := []int{8, 1, 2, 2, 3}
	fmt.Println(smallerNumbersThanCurrent(n))
}

func smallerNumbersThanCurrent(nums []int) []int {
	res := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		var cnt int
		for _, num := range nums {
			if num < nums[i] {
				cnt++
			}
		}
		res[i] = cnt
	}
	return res
}
