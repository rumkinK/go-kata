package main

import (
	"fmt"
)

func main() {
	sentences := []string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}
	fmt.Println(mostWordsFound(sentences))
}

func mostWordsFound(sentences []string) int {
	max := 0
	for _, sentence := range sentences {
		wrd := 0
		for _, s := range sentence {
			if s == ' ' {
				wrd++
			}
		}
		if len(sentence) > 0 {
			wrd++
		}
		if wrd > max {
			max = wrd
		}
	}
	return max
}
