package main

import (
	"fmt"
)

func main() {
	n := []int{1, 15, 6, 3}
	fmt.Println(differenceOfSum(n))
}

func differenceOfSum(nums []int) int {
	valuesSum := 0
	for i := 0; i < len(nums); i++ {
		valuesSum += nums[i]
	}
	digitSum := 0
	for i := 0; i < len(nums); i++ {
		num := nums[i]

		for num != 0 {
			digit := num % 10
			num /= 10
			digitSum += digit
		}
	}
	return valuesSum - digitSum
}
