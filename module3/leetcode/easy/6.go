package main

import "fmt"

func main() {
	words := []string{"yxmine"}
	fmt.Println(uniqueMorseRepresentations(words))
}

func uniqueMorseRepresentations(words []string) int {
	morzeUniq := map[string]int{}
	morze := map[string]string{
		"a": ".-", "b": "-...", "c": "-.-.", "d": "-..", "e": ".", "f": "..-.", "g": "--.", "h": "....", "i": "..", "j": ".---", "k": "-.-",
		"l": ".-..", "m": "--", "n": "-.", "o": "---", "p": ".--.", "q": "--.-", "r": ".-.", "s": "...", "t": "-", "u": "..-", "v": "...-", "w": ".--",
		"x": "-..-", "y": "-.--", "z": "--..",
	}
	for _, value := range words {
		morzeStr := ""
		for i := range value {
			liter := string(value[i])
			morzeStr += morze[liter]
		}
		morzeUniq[morzeStr]++
	}
	return len(morzeUniq)
}
