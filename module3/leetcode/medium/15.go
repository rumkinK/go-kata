package main

import (
	"fmt"
	"strconv"
)

func main() {
	n := "32"
	fmt.Println(minPartitions(n))
}

func minPartitions(n string) int {
	a, _ := strconv.Atoi(string(n[0]))
	for _, val := range n {
		num, _ := strconv.Atoi(string(val))
		if num > a {
			a = num
		}
	}
	return a
}
