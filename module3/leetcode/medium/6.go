package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	constructMaximumBinaryTree(nil)
}
func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	max, i := maxNum(nums)
	return &TreeNode{max, constructMaximumBinaryTree(nums[:i]), constructMaximumBinaryTree(nums[i+1:])}
}

func maxNum(nums []int) (max, index int) {
	for i, value := range nums {
		if i == 0 || value > max {
			max = value
			index = i
		}
	}
	return
}
