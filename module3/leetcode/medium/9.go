package main

import (
	"fmt"
	"sort"
)

func main() {
	n := []int{4, 6, 5, 9, 3, 7}
	l := []int{0, 0, 2}
	r := []int{2, 3, 5}
	fmt.Println(checkArithmeticSubarrays(n, l, r))
}

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	m := len(l)
	res := make([]bool, m)
	var buff func(arr []int) bool
	buff = func(arr []int) bool {
		diff := arr[0] - arr[1]
		for i := 2; i < len(arr); i++ {
			if arr[i-1]-arr[i] != diff {
				return false
			}
		}
		return true
	}
	for i := 0; i < m; i++ {
		tmp := make([]int, r[i]+1-l[i])
		copy(tmp, nums[l[i]:r[i]+1])
		sort.Ints(tmp)
		if buff(tmp) {
			res[i] = true
		} else {
			res[i] = false
		}
	}
	return res
}
