package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	bstToGst(nil)
}

func bstToGst(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	var temp []*TreeNode
	curr := root
	sum := 0
	for len(temp) != 0 || curr != nil {
		for curr != nil {
			temp = append(temp, curr)
			curr = curr.Right
		}
		curr, temp = temp[len(temp)-1], temp[:len(temp)-1]
		sum += curr.Val
		curr.Val = sum
		curr = curr.Left
	}
	return root
}
