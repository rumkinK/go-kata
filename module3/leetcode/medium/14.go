package main

import "fmt"

func main() {
	a := []int{1, 3, 4, 8}
	q := [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}}
	fmt.Println(xorQueries(a, q))
}
func xorQueries(arr []int, queries [][]int) []int {
	pr := make([]int, len(arr)+1, len(arr)+1)
	pr[0] = 0
	for i := 1; i < len(arr)+1; i++ {
		pr[i] = pr[i-1] ^ arr[i-1]
	}
	res := make([]int, len(queries))
	for i, q := range queries {
		res[i] = pr[q[0]] ^ pr[q[1]+1]
	}
	return res
}
