package main

import "fmt"

func main() {
	n := 6
	edge := [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}}
	fmt.Println(findSmallestSetOfVertices(n, edge))

}
func findSmallestSetOfVertices(n int, edges [][]int) []int {
	vert := make([]int, n)
	for _, value := range edges {
		vert[value[1]]++
	}
	res := make([]int, 0, n)
	for i, val := range vert {
		if val == 0 {
			res = append(res, i)
		}
	}
	return res
}
