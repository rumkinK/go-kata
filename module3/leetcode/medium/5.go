package main

import "math"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	pairSum(nil)
}

func pairSum(head *ListNode) int {
	result := 0
	slow := head
	fast := head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	var prev *ListNode
	for slow != nil {
		nextNode := slow.Next
		slow.Next = prev
		prev = slow
		slow = nextNode
	}
	for head != nil && prev != nil {
		val := head.Val + prev.Val
		result = int(math.Max(float64(val), float64(result)))
		head = head.Next
		prev = prev.Next
	}
	return result
}
