package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	averageOfSubtree(nil)
}
func averageOfSubtree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	rootSum, rootCount := sum(root)
	if root.Val == rootSum/rootCount {
		return 1 + averageOfSubtree(root.Left) + averageOfSubtree(root.Right)
	}
	return averageOfSubtree(root.Left) + averageOfSubtree(root.Right)
}

func sum(root *TreeNode) (int, int) {
	if root == nil {
		return 0, 0
	}
	leftSum, leftCount := sum(root.Left)
	rightSum, rightCount := sum(root.Right)
	return leftSum + rightSum + root.Val, leftCount + rightCount + 1
}
