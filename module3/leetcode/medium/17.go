package main

import "fmt"

func main() {
	var points = [][]int{{1, 3}, {3, 3}, {5, 3}, {2, 2}}
	var queries = [][]int{{2, 3, 1}, {4, 3, 1}, {1, 1, 2}}
	fmt.Println(countPoints(points, queries))
}
func countPoints(points [][]int, queries [][]int) []int {
	res := make([]int, len(queries))
	for i := range queries {
		sum := 0
		radiusSQR := queries[i][2] * queries[i][2]
		for j := range points {
			coord := ((points[j][0] - queries[i][0]) * (points[j][0] - queries[i][0])) + ((points[j][1] - queries[i][1]) * (points[j][1] - queries[i][1]))
			if coord <= radiusSQR {
				sum++
			}
		}
		res[i] = sum
	}
	return res
}
