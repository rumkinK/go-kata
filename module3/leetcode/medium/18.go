package main

import "fmt"

func main() {
	gr := []int{3, 3, 3, 3, 3, 1, 3}
	fmt.Println(groupThePeople(gr))
}

func groupThePeople(groupSizes []int) [][]int {
	res := make([][]int, 0)
	m := make(map[int][]int, 0)
	for key, value := range groupSizes {
		if _, ok := m[value]; !ok {
			m[value] = []int{key}
		} else {
			m[value] = append(m[value], key)
		}
		if value == len(m[value]) {
			res = append(res, m[value])
			m[value] = nil
		}
	}
	return res
}
