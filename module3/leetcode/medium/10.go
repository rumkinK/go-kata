package main

import "fmt"

func main() {
	fmt.Println(numTilePossibilities("AAABBC"))
}

func numTilePossibilities(tiles string) int {
	words := make(map[string]int)
	var queue []string
	for i := 0; i < len(tiles); i++ {
		char := string(tiles[i])
		length := len(queue)
		words[char]++
		queue = append(queue, char)
		for j := 0; j < length; j++ {
			for k := 0; k <= len(queue[j]); k++ {
				item := queue[j]
				item = item[:k] + char + item[k:]
				words[item]++
				queue = append(queue, item)
			}
		}
	}
	return len(words)
}
