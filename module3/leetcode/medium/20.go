package main

import "fmt"

func main() {
	qr := []int{3, 1, 2, 1}
	m := 5
	fmt.Println(processQueries(qr, m))
}

func processQueries(queries []int, m int) []int {
	res := []int{}
	per := make([]int, m, m)
	for i := 1; i <= m; i++ {
		per[i-1] = i
	}
	for _, qr := range queries {
		pos := 0
		for j := range per {
			if per[j] == qr {
				pos = j
				per = append(per[:j], per[j+1:]...)
				per = append([]int{qr}, per...)
				break
			}
		}
		res = append(res, pos)
	}
	return res
}
