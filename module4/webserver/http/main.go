package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		fmt.Fprintf(w, "GET request")
	case http.MethodPost:
		fmt.Fprintf(w, "POST request")
	default:
		http.Error(w, "Invalid request", http.StatusMethodNotAllowed)
	}
}
