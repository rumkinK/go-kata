package logger

import (
	"github.com/go-chi/chi/v5/middleware"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"time"
)

func New() logrus.FieldLogger {
	l := &logrus.Logger{
		Out:   os.Stdout,
		Level: logrus.InfoLevel,
	}
	l.SetFormatter(&logrus.TextFormatter{
		DisableColors:          false,
		DisableLevelTruncation: true,
		ForceColors:            true,
		FullTimestamp:          true,
		TimestampFormat:        "2006-01-02 15:04:05",
	})
	return l
}

func Logger(logger logrus.FieldLogger) func(h http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			mw := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
			t1 := time.Now()
			defer func() {
				deltaTime := time.Since(t1).String()
				fields := logrus.Fields{
					"d(t)":   deltaTime,
					"Status": mw.Status(),
				}
				if mw.Status() == 404 {
					logger.WithFields(fields).Warnf("%s %s", r.Method, r.RequestURI)
				} else {
					logger.WithFields(fields).Infof("%s %s", r.Method, r.RequestURI)
				}
			}()

			h.ServeHTTP(mw, r)
		}

		return http.HandlerFunc(fn)
	}
}
