package service

import "gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/models"

type UserService interface {
	GetAll() []models.User
	Get(id int) (models.User, error)
	Creat(user models.User) models.User
}

type userService struct {
	repo UserService
}

func NewUserService(u UserService) *userService {
	return &userService{repo: u}
}

func (u *userService) GetAll() []models.User {
	return u.repo.GetAll()
	//return u.repo.GetAll()
}

func (u *userService) Get(id int) (models.User, error) {
	return u.repo.Get(id)
}

func (u *userService) Creat(user models.User) models.User {
	return u.repo.Creat(user)
}
