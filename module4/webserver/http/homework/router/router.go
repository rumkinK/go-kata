package router

import (
	"github.com/go-chi/chi"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/handlers"
)

func RegisterRoutes(router chi.Router, h *handlers.UserHandler) {
	router.Get("/", h.Hello)
	router.Route("/users", func(r chi.Router) {
		r.Get("/", h.GetAll)
		r.Get("/{id}", h.Get)
		r.Post("/", h.Creat)
	})

	router.Route("/upload", func(r chi.Router) {
		r.Post("/", h.Upload)
		r.Get("/{fileName}", h.Download)
	})
}
