package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/models"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/service"
	"net/http"
	"strconv"
)

type UserHandler struct {
	repoService service.UserService
}

func New(rs service.UserService) *UserHandler {
	return &UserHandler{
		repoService: rs,
	}
}

func (h *UserHandler) Hello(w http.ResponseWriter, r *http.Request) {
	render.JSON(w, r, "Start page")
}

func (h *UserHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	users := h.repoService.GetAll()
	render.JSON(w, r, users)
}

func (h *UserHandler) Get(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(chi.URLParam(r, "id"))
	user, err := h.repoService.Get(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	render.JSON(w, r, user)
}

func (h *UserHandler) Creat(w http.ResponseWriter, r *http.Request) {
	var user models.User
	_ = json.NewDecoder(r.Body).Decode(&user)
	h.repoService.Creat(user)
	w.WriteHeader(http.StatusCreated)
}
