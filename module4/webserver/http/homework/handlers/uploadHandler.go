package handlers

import (
	"github.com/go-chi/chi"
	"io"
	"mime/multipart"
	"net/http"
	"os"
)

func (h *UserHandler) Upload(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(10 << 20) // 10 MB max file size
	if err != nil {
		http.Error(w, "Error parsing form data", http.StatusBadRequest)
		return
	}

	file, header, err := r.FormFile("file")
	if err != nil {
		http.Error(w, "Error retrieving file", http.StatusBadRequest)
		return
	}
	defer func(file multipart.File) {
		err := file.Close()
		if err != nil {
			http.Error(w, "Error closing file", http.StatusInternalServerError)
			return
		}
	}(file)

	out, err := os.Create("./public/" + header.Filename)
	if err != nil {
		http.Error(w, "Error creating file", http.StatusInternalServerError)
		return
	}
	defer func(out *os.File) {
		err := out.Close()
		if err != nil {
			http.Error(w, "Error closing file", http.StatusInternalServerError)
			return
		}
	}(out)

	_, err = io.Copy(out, file)
	if err != nil {
		http.Error(w, "Error copying file", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (h *UserHandler) Download(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Disposition", "attachment")
	path := "./public/" + chi.URLParam(r, "fileName")
	http.ServeFile(w, r, path)
}
