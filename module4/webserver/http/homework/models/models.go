package models

type User struct {
	ID   int    `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Mail string `json:"mail,omitempty"`
}
