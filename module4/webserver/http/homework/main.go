package main

import (
	"context"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/go-chi/chi"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/handlers"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/logger"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/models"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/repo"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/router"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/service"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	log := logger.New()
	repo := repo.NewUserRepo()
	service := service.NewUserService(repo)
	h := handlers.New(repo)

	users := generateUsers()
	for _, user := range users {
		service.Creat(user)
	}

	r := chi.NewRouter()
	r.Use(logger.Logger(log))
	router.RegisterRoutes(r, h)

	port := ":8080"
	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Warn("Shutdown Server ...")

	// Set shutdown time-out
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Info("Server exiting")

}

func generateUsers() []models.User {
	var users []models.User

	for i := 0; i < 10; i++ {
		var user models.User
		user.Name = gofakeit.Name()
		user.Mail = gofakeit.Email()

		users = append(users, user)
	}

	return users
}
