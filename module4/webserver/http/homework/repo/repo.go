package repo

import (
	"fmt"
	"gitlab.com/rumkinK/go-kata/module4/webserver/http/homework/models"
)

type UserRepo struct {
	users []models.User
}

func NewUserRepo() *UserRepo {
	return &UserRepo{users: []models.User{}}
}

func (u *UserRepo) GetAll() []models.User {
	return u.users
}

func (u *UserRepo) Get(id int) (models.User, error) {
	for _, user := range u.users {
		if user.ID == id {
			return user, nil
		}
	}
	err := fmt.Errorf("User ID: %v not found", id)
	return models.User{}, err
}

func (u *UserRepo) Creat(user models.User) models.User {
	user.ID = len(u.users) + 1
	u.users = append(u.users, user)
	return user
}
