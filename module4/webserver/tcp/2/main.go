package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func handleConnection(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)

	// Read the request
	request, _ := reader.ReadString('\n')
	fmt.Println(request)

	// Parse the request
	parts := strings.Split(request, " ")
	method := parts[0]
	path := parts[1]

	// Write the response
	if method == "GET" && path == "/" {
		fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\r\nHello, World!")
	} else {
		fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
	}
}

func main() {
	listener, _ := net.Listen("tcp", ":8080")
	defer listener.Close()

	for {
		conn, _ := listener.Accept()
		go handleConnection(conn)
	}
}
