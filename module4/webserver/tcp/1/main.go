package main

import (
	"fmt"
	"net"
)

func main() {
	// Слушаем порт 8080
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer ln.Close()

	for {
		// Принимаем входящие соединения
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}

		go func(conn net.Conn) {
			defer conn.Close()

			// Читаем входящие данные
			data := make([]byte, 1024)
			n, err := conn.Read(data)
			if err != nil {
				fmt.Println(err)
				return
			}

			// Печатаем полученные данные
			fmt.Println(string(data[:n]))
		}(conn)
	}
}
