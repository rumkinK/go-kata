module gitlab.com/rumkinK/go-kata

go 1.19

require (
	github.com/brianvoe/gofakeit/v6 v6.20.2
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-chi/render v1.0.2
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.1
	gitlab.com/rumkinK/greet v0.0.0-20230202152549-06f4ac7a53c3
	golang.org/x/sync v0.1.0
	pkg.re/essentialkaos/translit.v2 v2.0.3+incompatible
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	pkg.re/essentialkaos/check.v1 v1.2.0 // indirect
)
